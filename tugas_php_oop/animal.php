<?php 

    class Animal 
    {
        public $name;
        public $lengs = 4;
        public $cold_blooded = "no";

        public function __construct($kalimat)
        {
            $this->name = $kalimat;

        }

    }


/*
$sheep = new Animal("shaun");

echo $sheep->name; // "shaun"
echo $sheep->legs; // 4
echo $sheep->cold_blooded; // "no"

// NB: Boleh juga menggunakan method get (get_name(), get_legs(), get_cold_blooded())
*/

