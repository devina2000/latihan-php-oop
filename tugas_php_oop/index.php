<?php

    require_once('animal.php');
    require_once('frog.php');
    require_once('ape.php');

    $sheep = new Animal("shaun");
    
    echo "<br>";
    echo "Name Hewan : $sheep->name <br>";
    echo "legs : $sheep->lengs <br>";
    echo "Cold Blooded : $sheep->cold_blooded <br>";

    $kodok = new Frog("buduk");

    echo "<br>";
    echo " Name Hewan : $kodok->name <br>";
    echo " legs : $kodok->lengs <br>";
    echo " Cold Blooded : $kodok->cold_blooded <br>";
    $kodok->jump();

    $sungokong = new Ape("kera sakti");

    echo "<br><br>";
    echo " Name Hewan : $sungokong->name <br>";
    echo " legs : $sungokong->lengs <br>";
    echo " Cold Blooded : $sungokong->cold_blooded <br>";
    $sungokong->yell();


